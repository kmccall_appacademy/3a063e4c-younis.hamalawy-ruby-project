def echo(str)
  p str
end

def shout(str)
  p str.upcase
end

def repeat(str, num = 2)
  result = "#{str}"
  (num-1).times do
    result << " "
    result << str
  end
  result
end

def start_of_word(str, num)
  str[0..(num-1)]
end

def first_word(str)
  words = str.split
  words[0]
end

def titleize(str)
  result = []
  str.split.each_with_index do |word, idx|
    exceptions = ["a", "an" ,"the", "at", "by", "for", "from", "in", "into", "of", "off", "on", "onto", "out", "over", "and", "but", "or", "nor", "up" , "with",  "to" ,"as"]
    if idx == 0
      result << word.capitalize
    elsif exceptions.include?(word)
      result << word
    else
      result << word.capitalize
    end
  end
  result.join" "
end
