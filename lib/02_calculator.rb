def add(first_num, second_num)
  result = first_num + second_num
end

def subtract(first_num, second_num)
  result = first_num - second_num
end

def sum(array)
  result = array.inject(0, :+)
end

def multiply(array)
  result = array.inject(:*)
end

def power(first_num,second_num)
  result = first_num ** second_num
end

def factorial(num)
  return 1 if num == 0
  result = (1..num).to_a.inject(:*)
end
